package com.example.swbr


import android.content.Context
import androidx.annotation.WorkerThread
import androidx.work.Data
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.google.gson.Gson
import com.google.gson.JsonObject
import java.net.HttpURLConnection
import java.net.URL

class CatFactWorker(context: Context, params: WorkerParameters) : Worker(context, params) {

    private val apiUrl = "https://catfact.ninja/facts?limit=25"

    @WorkerThread
    override fun doWork(): Result {
        val catFacts = fetchCatFacts()
        Thread.sleep(2500)
        val outputData = Data.Builder()
            .putString("catFacts", Gson().toJson(catFacts))
            .build()
        return Result.success(outputData)
    }

    private fun fetchCatFacts(): List<CatFact> {
        var catFacts: List<CatFact> = emptyList()
        try {
            val url = URL(apiUrl)
            val connection = url.openConnection() as HttpURLConnection
            connection.requestMethod = "GET"
            connection.connectTimeout = 10000
            val response = connection.inputStream.bufferedReader().readText()
            connection.disconnect()
            val json = Gson().fromJson(response, JsonObject::class.java)
            val data = json.getAsJsonArray("data")
            catFacts = data.map {
                val factObject = it.asJsonObject
                CatFact(
                    fact = factObject.get("fact").asString,
                    length = factObject.get("length").asInt
                )
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return catFacts
    }
}