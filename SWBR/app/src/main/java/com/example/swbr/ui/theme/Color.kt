package com.example.swbr.ui.theme

import androidx.compose.ui.graphics.Color

val Purple80 = Color(0xFFD0BCAA)
val PurpleGrey80 = Color(0xFF1150C1)
val Pink80 = Color(0xFFEFA4C1)

val Purple40 = Color(0xFF1150C1)
val PurpleGrey40 = Color(0xFF256b71)
val Pink40 = Color(0xFF7D5261)