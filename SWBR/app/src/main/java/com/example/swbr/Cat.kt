package com.example.swbr

data class CatFact(
    val fact: String,
    val length: Int
)
